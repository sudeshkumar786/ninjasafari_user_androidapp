package com.ninja.ui.activity.splash;

import com.ninja.base.MvpView;
import com.ninja.data.network.model.User;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface SplashIView extends MvpView{
    void onSuccess(User user);
    void onError(Throwable e);
}
