package com.ninja.ui.activity.coupon;

import com.ninja.base.MvpView;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface CouponIView extends MvpView {
    void onSuccess(Object object);
    void onError(Throwable e);
}
