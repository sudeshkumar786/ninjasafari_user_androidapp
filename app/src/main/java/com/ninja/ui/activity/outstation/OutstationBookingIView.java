package com.ninja.ui.activity.outstation;


import com.ninja.base.MvpView;
import com.ninja.data.network.model.EstimateFare;
import com.ninja.ui.adapter.ServiceAdapterSingle;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface OutstationBookingIView extends MvpView {

    void onSuccess(ServiceAdapterSingle adapter);

    void onSuccessRequest(Object object);
    void onSuccess(EstimateFare estimateFare);
}
