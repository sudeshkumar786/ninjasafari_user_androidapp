package com.ninja.ui.activity.card;

import com.ninja.base.MvpPresenter;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface CarsIPresenter<V extends CardsIView> extends MvpPresenter<V> {
    void card();
}
