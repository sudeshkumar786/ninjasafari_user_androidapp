package com.ninja.ui.activity.help;

import com.ninja.base.MvpView;
import com.ninja.data.network.model.Help;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface HelpIView extends MvpView {
    void onSuccess(Help help);
    void onError(Throwable e);
}
