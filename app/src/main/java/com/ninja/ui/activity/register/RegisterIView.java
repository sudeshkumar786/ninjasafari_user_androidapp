package com.ninja.ui.activity.register;

import com.ninja.base.MvpView;
import com.ninja.data.network.model.MyOTP;
import com.ninja.data.network.model.SettingsResponse;
import com.ninja.data.network.model.Token;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface RegisterIView extends MvpView{
    void onSuccess(Token token);
    void onSuccess(MyOTP otp);
    void onError(Throwable e);
    void onSuccess(SettingsResponse response);
}
