package com.ninja.ui.activity.otp;

import com.ninja.base.MvpView;
import com.ninja.data.network.model.MyOTP;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface OTPIView extends MvpView{
    void onSuccess(MyOTP otp);
    void onError(Throwable e);
}
