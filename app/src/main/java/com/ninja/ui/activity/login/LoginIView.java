package com.ninja.ui.activity.login;

import com.ninja.base.MvpView;
import com.ninja.data.network.model.ForgotResponse;
import com.ninja.data.network.model.Token;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface LoginIView extends MvpView{
    void onSuccess(Token token);
    void onSuccess(ForgotResponse object);
    void onError(Throwable e);
}
