package com.ninja.ui.fragment.invoice;

import com.ninja.base.MvpView;
import com.ninja.data.network.model.Message;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface InvoiceIView extends MvpView{
    void onSuccess(Message message);
    void onError(Throwable e);
}
