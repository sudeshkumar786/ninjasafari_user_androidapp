package com.ninja.base;

import android.app.Activity;

import com.ninja.MvpApplication;


public interface MvpPresenter<V extends MvpView> {
    Activity activity();
    MvpApplication appContext();
    void attachView(V mvpView);

    void detachView();

}
